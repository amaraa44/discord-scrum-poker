package hu.davidhalma.scrumpoker.exception

import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.entities.MessageChannel
import java.awt.Color
import java.time.Instant

class AlreadySelectedException(message: String?) : Exception(message) {

    constructor(event: MessageChannel, message: String?) : this(message) {
        val embedBuilder = EmbedBuilder()
            .setTitle(message)
            .setColor(Color.RED)
            .setTimestamp(Instant.now())
        event.sendMessage(embedBuilder.build()).queue()
    }

}