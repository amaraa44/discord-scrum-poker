package hu.davidhalma.scrumpoker.model

data class VoteResult(val authorName: String?, val storyPoint: Int?)
