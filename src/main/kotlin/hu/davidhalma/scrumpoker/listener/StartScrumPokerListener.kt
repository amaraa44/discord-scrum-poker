package hu.davidhalma.scrumpoker.listener

import hu.davidhalma.scrumpoker.converter.VoteResultConverter
import hu.davidhalma.scrumpoker.exception.NoRunningPoll
import hu.davidhalma.scrumpoker.model.Vote
import hu.davidhalma.scrumpoker.service.CommandLineService
import hu.davidhalma.scrumpoker.service.CommandLineService.Command.*
import net.dv8tion.jda.api.EmbedBuilder
import net.dv8tion.jda.api.events.message.MessageReceivedEvent
import net.dv8tion.jda.api.hooks.ListenerAdapter
import org.apache.commons.cli.CommandLine
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.awt.Color
import java.time.Instant


@Service
class StartScrumPokerListener : ListenerAdapter() {

    private val log = LogManager.getLogger(this::class.java)

    @Autowired
    private lateinit var commandLineService: CommandLineService

    @Autowired
    private lateinit var voteResultConverter: VoteResultConverter

    private val voteRunningUntil = mutableMapOf<String, Int>()
    private val runningPolls = mutableMapOf<String, Boolean>()
    private val votes = mutableListOf<Vote>()

    @Value("\${app.prefix}")
    private var prefix: String = "!sp"

    @Value("\${app.mentionChannel}")
    private var mentionChannel: Boolean = false

    @Value("\${app.mentionUser}")
    private var mentionUser: Boolean = false

    private var color: Color = Color.MAGENTA

    override fun onMessageReceived(event: MessageReceivedEvent) {
        if (event.message.contentRaw.startsWith(prefix)) {
            log.info("Channel: ${event.channel} | User: ${event.author.name} | Message: ${event.message.contentRaw}")
            val command = commandLineService.commandLine(event)
            when {
                command.hasOption(START.opt) -> start(event, command)
                command.hasOption(END.opt) -> stop(event)
                command.hasOption(RESTART.opt) -> restart(event)
                else -> help(event, command)
            }
        } else if (isVoteMessage(event)) {
            event.message.delete().queue()
            if (mentionUser) {
                event.channel.sendMessage("<@${event.author.id}> voted.").queue()
            } else {
                event.channel.sendMessage("${event.author.name} voted.")
            }

            val currentVote = Vote(event.channel.id, event.author.name, event.message.contentRaw.toInt())
            if (votes.find { it.channelId == currentVote.channelId && it.authorName == event.author.name } == null) {
                votes.add(currentVote)
            } else {
                votes.removeIf { it.channelId == currentVote.channelId && it.authorName == event.author.name }
                votes.add(currentVote)
            }

            if (voteRunningUntil[event.channel.id] != null && voteRunningUntil[event.channel.id]!! <= votes.filter { it.channelId == event.channel.id }.count()) {
                stop(event)
            }
        }
    }

    fun start(event: MessageReceivedEvent, command: CommandLine) {
        runningPolls[event.channel.id] = true
        voteRunningUntil[event.channel.id] = command.getOptionValue(VOTERS.opt, "50").toInt()
        val embedBuilder = EmbedBuilder()
        embedBuilder.setTitle("Scrum Poker started.")
        var title: String? = null
        val titleArray = command.getOptionValues(TITLE.opt)
        if (null != titleArray) title = titleArray.joinToString(" ")
        title = if (mentionChannel && title != null) {
            "<#${event.channel.id}> " + title
        }else {
            "<#${event.channel.id}>"
        }
        embedBuilder.setDescription(title)
        embedBuilder.setColor(color)
        embedBuilder.setTimestamp(Instant.now())
        embedBuilder.setFooter("To stop running scrum poker send '$prefix --${END.longOpt}'. For more help send '$prefix --${HELP.longOpt}'")
        event.channel.sendMessage(embedBuilder.build()).queue()
        log.info("Scrum Poker started.")
    }

    private fun stop(event: MessageReceivedEvent) {
        runningPolls.remove(event.channel.id)
        voteRunningUntil.remove(event.channel.id)

        val votes = votes.filter { it.channelId == event.channel.id }
        val results = voteResultConverter.from(votes)
        log.info("Scrum Poker ended on channel: '${event.channel.name}' (${event.channel.id}). Results: $results")
        val embedBuilder = EmbedBuilder()
        if (results.isEmpty()) {
            embedBuilder.setTitle("No result. No one voted.")
            embedBuilder.setColor(color)
        } else {
            embedBuilder.setTitle("Scrum Poker results")
            embedBuilder.setColor(color)
            embedBuilder.setTimestamp(Instant.now())
            results.map { embedBuilder.addField(it.authorName, it.storyPoint.toString(), true) }
        }
        embedBuilder.setFooter("For more help send '$prefix --${HELP.longOpt}'")
        event.channel.sendMessage(embedBuilder.build()).queue()
        this.votes.removeIf { it.channelId == event.channel.id }
    }

    private fun restart(event: MessageReceivedEvent) {
        if (!runningPolls.containsKey(event.channel.id)) {
            log.warn("Restarted but there are currently no polls running.")
            throw NoRunningPoll(event.channel, "There are currently no polls running.")
        }
        this.votes.removeIf { it.channelId == event.channel.id }
        val embedBuilder = EmbedBuilder()
            .setTitle("Scrum Poker restarted.")
            .setColor(color)
            .setTimestamp(Instant.now())
        event.channel.sendMessage(embedBuilder.build()).queue()
    }

    private fun help(event: MessageReceivedEvent, command: CommandLine?) {
        if (command != null) {
            val embedBuilder = EmbedBuilder()
            embedBuilder.setTitle("Scrum Poker help.")
            embedBuilder.setColor(color)
            embedBuilder.setTimestamp(Instant.now())
            embedBuilder.addField("Usage", commandLineService.getUsage(), false)
            for (line in commandLineService.getHelp()) {
                val split = line.split(Regex("<arg>|\\s<arg>\\s|\\s{2,}"))
                embedBuilder.addField(split[0], split[1], false)
            }
            event.channel.sendMessage(embedBuilder.build()).queue()
        } else {
            event.channel.sendMessage(EmbedBuilder().setTitle("Something went wrong.").setColor(Color.RED).build()).queue()
        }
    }

    private fun isVoteMessage(event: MessageReceivedEvent) = runningPolls[event.channel.id] ?: false && !event.author.isBot
}
