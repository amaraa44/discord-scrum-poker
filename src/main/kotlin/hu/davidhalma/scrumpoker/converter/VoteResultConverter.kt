package hu.davidhalma.scrumpoker.converter

import hu.davidhalma.scrumpoker.model.Vote
import hu.davidhalma.scrumpoker.model.VoteResult
import org.springframework.stereotype.Service

@Service
class VoteResultConverter {

    fun from(votes: List<Vote>) = votes.map { from(it) }
    fun from(vote: Vote) = VoteResult(vote.authorName, vote.storyPoint)

}