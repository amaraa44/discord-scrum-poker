package hu.davidhalma.scrumpoker.config

import hu.davidhalma.scrumpoker.listener.StartScrumPokerListener
import net.dv8tion.jda.api.JDA
import net.dv8tion.jda.api.JDABuilder
import org.apache.logging.log4j.LogManager
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class JDAConfig {

    private val log = LogManager.getLogger()

    @Value("\${app.token}")
    private lateinit var token: String

    @Autowired
    private lateinit var startScrumPokerListener: StartScrumPokerListener

    @Bean
    fun discordAPI(): JDA {
        val jda = JDABuilder.createDefault(token).build()
        jda.addEventListener(startScrumPokerListener)
        log.info("Scrum Poker is alive.")
        return jda
    }
    
}