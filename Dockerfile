FROM gradle:6.4.1-jdk11 AS builder
WORKDIR /app
COPY . /app
RUN gradle clean build

FROM openjdk:11-jdk-slim
COPY --from=builder app/build/libs/scrumpoker.jar /app.jar
CMD ["java", "-jar", "/app.jar"]